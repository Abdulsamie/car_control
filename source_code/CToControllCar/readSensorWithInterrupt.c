/* 
test 1:
#include "vl53l1_api.h"
#include <poll.h>


static VL53L1_Dev_t device;
static VL53L1_Error status;

int main(int argc, char** argv) {
    // Initialize the device
    status = VL53L1_WaitDeviceBooted(&device);
    if (status != VL53L1_ERROR_NONE) {
        printf("Error: VL53L1_WaitDeviceBooted() returned %d\n", status);
        return 1;
    }

    // Configure the device
    status = VL53L1_DataInit(&device);
    if (status != VL53L1_ERROR_NONE) {
        printf("Error: VL53L1_DataInit() returned %d\n", status);
        return 1;
    }

   // Configure the interrupt
    int interrupt_pin = // use the right pin number here //
    VL53L1_GPIO_t interrupt = {
        .gpio_number = interrupt_pin,
        .gpio_type = VL53L1_GPIOFUNCTIONALITY_THRESHOLD_CROSSED_LOW
    };
    VL53L1_SetGpioConfig(&device, VL53L1_DEVICEMODE_SINGLE_RANGING, &interrupt);


    // Start continuous ranging
    status = VL53L1_StartMeasurement(&device);
    if (status != VL53L1_ERROR_NONE) {
        printf("Error: VL53L1_StartMeasurement() returned %d\n", status);
        return 1;
    }

    struct pollfd pfd;
    pfd.fd = interrupt_pin;
    pfd.events = POLLPRI;
    while (1) {
    	// Wait for interrupt
        poll(&pfd, 1, -1);
        lseek(pfd.fd, 0, SEEK_SET);
        char buf[1];
        read(pfd.fd, buf, 1);

	// Read the data
        VL53L1_RangingMeasurementData_t data;
        status = VL53L1_GetRangingMeasurementData(&device, &data);
        if (status != VL53L1_ERROR_NONE) {
            printf("Error: VL53L1_GetRangingMeasurementData() returned %d\n", status);
            return 1;
        }

     // Print the distance
        printf("Distance: %d mm\n", data.RangeMilliMeter);
	}

 return 0;

}
*/



// to test 2:
/*
#include "vl53l1_api.h"
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

static VL53L1_Dev_t device;
static VL53L1_Error status;

int main(int argc, char** argv) {
    // Initialize the device
    status = VL53L1_WaitDeviceBooted(&device);
    if (status != VL53L1_ERROR_NONE) {
        printf("Error: VL53L1_WaitDeviceBooted() returned %d\n", status);
        return 1;
    }

    // Configure the device
    status = VL53L1_DataInit(&device);
    if (status != VL53L1_ERROR_NONE) {
        printf("Error: VL53L1_DataInit() returned %d\n", status);
        return 1;
    }

    // Configure the interrupt
    int interrupt_pin = // use the right pin number here //
    VL53L1_GPIO_t interrupt = {
        .gpio_number = interrupt_pin,
        .gpio_type = VL53L1_GPIOFUNCTIONALITY_THRESHOLD_CROSSED_LOW
    };
    VL53L1_SetGpioConfig(&device, VL53L1_DEVICEMODE_SINGLE_RANGING, &interrupt);

    // Open the interrupt pin for reading
    int fd = open("/sys/class/gpio/gpio" + interrupt_pin + "/value", O_RDONLY);
    if (fd == -1) {
        perror("Error opening interrupt pin");
        return 1;
    }

    // Start continuous ranging
    status = VL53L1_StartMeasurement(&device);
    if (status != VL53L1_ERROR_NONE) {
        printf("Error: VL53L1_StartMeasurement() returned %d\n", status);
        return 1;
    }

    while (1) {
        // Wait for interrupt
        char buf[1];
        int n = read(fd, buf, 1);
        if (n == -1) {
            perror("Error reading interrupt pin");
            return 1;
        }

        // Read the data
        VL53L1_RangingMeasurementData_t data;
        status = VL53L1_GetRangingMeasurementData(&device, &data);
        if (status != VL53L1_ERROR_NONE) {
            printf("Error: VL53L1_GetRangingMeasurementData() returned %d\n", status);
            return 1;
        }

        // Print the distance
        printf("Distance: %d mm\n", data.RangeMilliMeter);
    }

    return 0;
}
*/
