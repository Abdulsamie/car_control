const http = require('http');
const { exec } = require('child_process');
const express = require('express');
const app = express();
const fs = require('fs');
const gpios = [17, 27, 10, 9, 11, 0];

async function gpio_export() {
// Export the GPIO pins for the motors
  await gpios.forEach(gpi => {
  exec(`echo ${gpi} > /sys/class/gpio/export`)
});

// Set the direction of the GPIO
  await gpios.forEach(gpi => {
  exec(`echo out > /sys/class/gpio/gpio${gpi}/direction`)
});
}

async function gpio_unexport() {
  await gpios.forEach(gpi => {
  exec(`echo ${gpi} > /sys/class/gpio/unexport`)
});
}

async function forward_start() {
  await exec("echo 1 > /sys/class/gpio/gpio17/value")
  await exec("echo 1 > /sys/class/gpio/gpio10/value")
}

async function backward_start() {
  await exec("echo 1 > /sys/class/gpio/gpio27/value")
  await exec("echo 1 > /sys/class/gpio/gpio9/value")
}

async function left_start() {
  await exec("echo 1 > /sys/class/gpio/gpio0/value")
}

async function right_start() {
  await exec("echo 1 > /sys/class/gpio/gpio11/value")
}

async function forward_stop() {
  await exec("echo 0 > /sys/class/gpio/gpio17/value")
  await exec("echo 0 > /sys/class/gpio/gpio10/value")
  }

async function backward_stop() {
  await exec("echo 0 > /sys/class/gpio/gpio27/value")
  await exec("echo 0 > /sys/class/gpio/gpio9/value")
  }

async function left_stop() {
  await exec("echo 0 > /sys/class/gpio/gpio0/value")
}

async function right_stop() {
  await exec("echo 0 > /sys/class/gpio/gpio11/value")
}

const server = http.createServer(async (req, res) => {
   if (req.url === '/' && req.method === 'GET') {
        await gpio_export()
        const html = fs.readFileSync('./public/car.html', 'utf-8');
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.end(html);
  } else if (req.url === '/forward_start' && req.method === 'GET') {
        await forward_start();
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end('Car is moving forward now!');
  } else if (req.url === '/backward_start' && req.method === 'GET') {
        await backward_start();
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end('Car is moving backward now!');
  } else if (req.url === '/left_start' && req.method === 'GET') {
        await left_start();
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end('Car is turning left now!');
  } else if (req.url === '/right_start' && req.method === 'GET') {
        await right_start();
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end('Car is turning right now!');
  } else if (req.url === '/forward_stop' && req.method === 'GET') {
        await forward_stop();
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end('Car stopped moving forward');
  } else if (req.url === '/backward_stop' && req.method === 'GET') {
        await backward_stop();
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end('Car stopped moving backward');
  } else if (req.url === '/left_stop' && req.method === 'GET') {
        await left_stop();
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end('Car stopped moving left');
  } else if (req.url === '/right_stop' && req.method === 'GET') {
        await right_stop();
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end('Car stopped moving right');
  }  else if (req.url === '/stop') {
        await gpio_unexport();
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end('all gpios unexported');
  }  else {
        res.writeHead(404, { 'Content-Type': 'text/plain' });
        res.end('404 Not Found');
  }
});

server.listen(8080 , () => {
console.log('Server started on http://192.168.2.74:8080');
});