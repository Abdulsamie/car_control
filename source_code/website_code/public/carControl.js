   // <script>
      const forwardButton = document.getElementById('forward-button');
      const backwardButton = document.getElementById('backward-button');
      const leftButton = document.getElementById('left-button');
      const rightButton = document.getElementById('right-button');

      forwardButton.addEventListener('click', () => {
        fetch('http://192.168.2.74:8080/forward');
      });

      backwardButton.addEventListener('click', () => {
        fetch('http://192.168.2.74:8080/backward');
      });

      leftButton.addEventListener('click', () => {
        fetch('http://raspberrypi.local:8080/left');
      });

      rightButton.addEventListener('click', () => {
        fetch('http://raspberrypi.local:8080/right');
      });
   // </script>



document.addEventListener('keydown', (event) => {
  switch (event.code) {
    case "ArrowUp":
      fetch('http://192.168.2.74:8080/forward');
      break;
    case "ArrowDown":
      fetch('http://192.168.2.74:8080/backward');
      break;
    case "ArrowLeft":
      fetch('http://192.168.2.74:8080/left');
      break;
    case "ArrowRight":
      fetch('http://192.168.2.74:8080/right');
      break;
  }
});
