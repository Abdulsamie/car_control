To be able to run the program at boot time or at startup, we need to do the following.

1. Create a script that starts your C program, for example,
start_my_program.sh:
chmod +x start_my_program.sh

2. Create a new systemd service file my_program.service in the /etc/systemd/system directory:

[Unit]
Description=My Program

[Service]
ExecStart=/bin/bash /path/to/start_my_program.sh
Restart=always
User=root

[Install]
WantedBy=multi-user.target

3. Reload the systemd service files by running the command: systemctl daemon-reload.

4. Enable the service by running the command: systemctl enable my_program.service.

5. Start the service by running the command: systemctl start my_program.service.



