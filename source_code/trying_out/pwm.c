#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
static int write_pwm_config( char *fname, char *value )
{
  int fd_pwm, ret, length;
  char fpath[128];
  if (fname==NULL || value==NULL) {
    return -1;
  }
  snprintf( fpath, sizeof(fpath), "/sys/class/pwm/pwmchip0/%s", fname);
  length = strlen( fpath );
  if (length >= sizeof(fpath)) {
    fprintf( stderr, "path %s too long\n", fname);
    return -1;
  }
  fd_pwm = open( fpath, O_WRONLY );
  if (fd_pwm<0) {
    perror( fpath );
    return -1;
  }
  ret = write(fd_pwm, value, strlen(value));
  if (ret<0) {
    perror( value );
    close( fd_pwm );
    return -2;
  }
  close( fd_pwm );
  return 0;
}
int main( int argc, char **argv )
{
  int ret;
  ret = write_pwm_config("unexport","0");
  if (ret) return ret;
  ret = write_pwm_config("export","0");
  if (ret) return ret;
  ret = write_pwm_config("pwm0/period", "500000000");
  if (ret) return ret;
  ret = write_pwm_config("pwm0/duty_cycle", "400000000");
  if (ret) return ret;
  ret = write_pwm_config("pwm0/enable", "1");
  if (ret) return ret;
  return 0;
}
