# 2022WS_team1
Welcome to the repository for Group A's Omniwheel project. This project was done for the C71 CSE course.

The project can be cloned from the command line via ``Git SSH protocol``,
whereby the own public key must be uploaded to GitLab beforehand:

```
 $ git clone git@gitlab.rz.htw-berlin.de:c71_cse/2022ws_team1.git
 $ cd 2022WS_teamA
```

Alternatively you can clone the repository via ``https`` protocol, then you have to enter the
HTW credentials must be entered:

```
 $ git clone https://gitlab.rz.htw-berlin.de/c71_cse/2022ws_team1.git
 $ cd 2022WS_teamA
```
You can also clone a specific branch via the following command: 
```
 $ git clone -b Branch_Name https://gitlab.rz.htw-berlin.de/c71_cse/2022ws_team1.git
 $ cd 2022WS_teamA
```

Project structure
---------------------

 * ```Requirements```: All the technical requirements are listed under [Requirements](https://gitlab.htw-berlin.de/c71_cse/2022ws_team1/-/requirements_management/requirements).


 * ```Source code```: The Repository itself is used for technical artifacts (files such as source code, drawings, models etc.). Our source code can be found in the directory [source_code](https://gitlab.htw-berlin.de/c71_cse/2022ws_team1/-/tree/main/source_code). 

 * ```Documentation```: (explanations, concepts...) is stored in the project [Wiki](https://gitlab.htw-berlin.de/c71_cse/2022ws_team1/-/wikis/home). The project wiki shall is structured according to project phases so you can find information easily.

* ```Presentations```: The presentations are stored in the [documentation](https://gitlab.htw-berlin.de/c71_cse/2022ws_team1/-/tree/main/documentation) folder in the repository.


Update project locally
------
To update main branch
```
 $ cd 2022WS_teamA
 $ git pull origin main
```
Or update a specific branch
git pull origin my-branch
```
 $ cd 2022WS_teamA
 $ git pull origin Branch_Name
```

Questions
------
Questions can be asked here via
[Issues](https://gitlab.rz.htw-berlin.de/c71_cse/2022ws_team1/-/issues)
can be asked.


